# Image Manipulation and Generation

* [Reference-Conditioned Super-Resolution by Neural Texture Transfer](https://web.eecs.utk.edu/~zzhang61/project_page/SRNTT/SRNTT.html)

## GAN Generation Demos

* [Ganbreeder](https://ganbreeder.app/)
* [Which Person Exists?](http://www.whichfaceisreal.com/)
* [This Person Does Not Exist](https://thispersondoesnotexist.com/)
* [This Cat Does Not Exist](https://thiscatdoesnotexist.com/)
* [This Waifu Does Not Exist](http://www.thiswaifudoesnotexist.net/)
* [This AirBnB Does Not Exist](https://thisairbnbdoesnotexist.com/)
