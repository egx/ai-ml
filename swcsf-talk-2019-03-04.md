# Southwest Cyber Security Forum Talk Notes

These are notes and reference materials from the AI and ML talk given by Elio Grieco at the 2019-03-04 Southwest Cyber Security Forum.

## Terms

AI = Artificial Intelligence
ML = Machine Learning
Dimensionality = The number of different aspects/facets of a data set e.g. medical records contain height, weight, age, blood type, etc.

## What are ML and AI?

Techniques that find complex patterns in large quantities of high-dimensionality data and convert them into lower dimensionality mappings.

ML happens in R, Octave, MatLab, Python, etc.

AI happens in PowerPoint. **AI is a marketing term.**

While there are dozens of ML techniques, each solving certain classes of problems with varying levels of effectiveness, none of them can be considered intelligent.

## What are their applications?

ML is amazingly good at finding patterns, especially when the amount of data we need to examine exceeds that which a human can reliably review in a career.

Examples:

* Image classification
* Diagnostic radiography
* Legal analysis
* Image enhancement (repair, upscaling)

### What problems do they solve well?

Any task that is repetitive with variation.

### What problems can they solve acceptably?

Any task which is repetitive 

### Can techniques be combined?

Yes. Certain ML techniques excel at solving certain problems.

* K-means and Self Organizing Maps (SOMs) - excel at clustering
* Long short-term memory (LSTM) - can see changes over time
* Generative Adversarial Networks (GANs) - are excellent at generating data based on previous inputs (like Markov chains but more sophisticated)

## What are their weaknesses?

### None of this is actually intelligent!

While finding patterns is certainly part of intelligence, it is not nearly all of intelligence.

It should be noted that there is not currently a language for describing a intelligence in general terms. Thus, we cannot (except by accident) possibly build an intelligence.

### What problems do they solve poorly?

Any task that requires **understanding of meaning**, **judgement**, **common sense** is current outside the domain of ML.

### What are their worst failure modes?

* Overfitting
* Failure to handle spatial translations
* Incorrect feature extraction
* Bad cost functions and cheating algorithms
* Encoded or derived bias (see below)

## How will they affect society?

### Job market?

While the industrial revolution brought with it tremendous leaps forward in automating many highly repetitive jobs, most jobs were still beyond automation. Further, keeping dumb robots working in a world of entropy creates many new jobs.

ML allows us to address the entropy and variations that stymie dumb automation e.g. pick and pack used to require precise alignment of goods so that when robots closed their graspers, something was underneath. Today's pick and pack systems use computer vision to determine positioning and alignment of items strewn haphazardly onto a feed conveyor. They are able to pick them off the conveyor and precisely place them into final packaging.

More worryingly, jobs that were formerly immune to automation like medical, legal, and other diagnostic jobs are already being successfully automated with modern ML techniques.

### Education?

Today's educational system is still heavily based on the needs of the industrial age, lots of manpower to do menial and repetitive tasks. This is clearly seen in the structure of the system:

* Show up on time (the whole assembly line must be present to operate)
* Heavy emphasis on repetition (computation is taught rather than mathematics more broadly)
* Metrics are fetishized (processes can only be improved if they can be measured)

The problem is that these needs will no longer be relevant for the majority of the workforce in the age of Automation and Augmentation. All of the repetitive work will be automated leaving only jobs that require a different set of skills:

* Imagination and creativity
* Communication and collaboration with team members
* Abstract thinking and problem solving

A very relevant book:

* [Robot-Proof | The MIT Press](https://mitpress.mit.edu/books/robot-proof)
* [Robot-Proof: A Conversation](https://www.youtube.com/watch?v=gi3sBk2L9HI)
* [Robot-Proof: higher education in the age of artificial intelligence](https://www.youtube.com/watch?v=jk4XjTi1hYo)

### Technological Progress?

The rate of innovation should increase as we can now hand off the drudgery of innovation to the machines.

A very time consuming aspect of innovation and problem solving is finding patterns. This will be trivial with the application of ML techniques.

Creativity can also be enhanced. Generative ML techniques don't think like humans and can therefore augment the designer in exploring the full problem space by stepping outside their preconceived notions and biases.

### Psychological and sociological implications

While the ability to outsource the last vestiges of manual drudgery to the machines should usher in a new age of leisure, there are downsides (Click Movie).



### Implications with regard to hybrid warfare

Fake news is already a huge problem, it will get **much worse** with the broad availability of ML techniques.

#### Deep Fakes

Now that the [deepfakes/faceswap](https://github.com/deepfakes/faceswap) source code (unofficial) has been presented to the world it's going to get a lot harder to believe video.

Similar transfer learning projects exist for voice and can be created for just about any kind of data using similar techniques.

#### This X Does Not Exist

Full fabrication of entities is already good enough to fool a casual observer:

* [This Person Does Not Exist](https://thispersondoesnotexist.com/)
* [Which Face is Real?](http://www.whichfaceisreal.com/)
* [This Cat Does Not Exist](https://thiscatdoesnotexist.com/)
* [This Waifu Does Not Exist](https://www.thiswaifudoesnotexist.net/)
* [This Rental Does Not Exist](https://thisrentaldoesnotexist.com/about/)
* [This Startup Does Not Exist](https://thisstartupdoesnotexist.com/)

#### Upsides Deep Fakes

The upside is that there is a brand new excuse to fight revenge porn and extortion attempts: **Fake Nudes!**

Since no picture can be trusted, any picture can be denied as a falsified/generated until proof to the contrary is presented.

## What are the potential dangers?

### Algorithmic bias

While math may be objective and unbiased, the humans applying it never are. This is even more true of ML systems which can become biased via multiple effects:

* Bias in design and selected training dataset
* Societal biases present in the training set
* Biases of designers or trainers in supervised learning
* "Optimizations" due to poorly specified cost functions

### Algorithmic opacity and predictability

ML techniques are currently opaque. They make decisions and we don't know why, nor can we ask them to justify their decision making process. The first work I've seen in regard to this problem is:

[Lime](https://github.com/marcotcr/lime): Explaining the predictions of any machine learning classifier

#### Catastrophic Interference

Further, ML algorithms are vulnerable to catastrophic interference (a.k.a. catastrophic forgetting). As training data is inserted, it can interfere with itself and the system can abruptly lose the ability to recognize items that it used to recognize with high confidence.

### Cheating algorithms and unexpected behaviors

Cost functions are critically important. Choosing the wrong function can lead to "cheating AIs"

### Dangers of adversarial ML and AI attacks

ML techniques are fundamentally "fuzzy" i.e. they are not a solid True or False, but more of a percentage confidence. This can be dangerous for things like authentication (though the same could be said of passwords and authentication in general).

#### Segmentation Attacks

Many ML techniques based on images pre-segment the image to find the "interesting parts" and thus reduce computational load. This can be abused to make the system ignore things it should catch.
