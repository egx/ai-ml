# Interesting projects that are currently focused on AI/ML implementation

* [AdviNow - Integrating AI and Augmented Reality into Patient Care](https://advinow.com/)
* [This Person Does Not Exist - Photos of people made using AI](https://thispersondoesnotexist.com/)
* [This Sartup Does Not Exist - Startup site pages created by AI](https://thisstartupdoesnotexist.com/)
