# Disappearing Jobs

* [Why the tech sector may not solve America’s looming automation crisis](https://pudding.cool/2018/08/retraining/)

## Counter-arguments

* [How AI automation could boost employment: The role of demand](https://bitsandatoms.co/how-ai-automation-could-boost-employment-the-role-of-demand/)
  * However, the above argument is shown to be poorly thought out in [The Rise of the Machines – Why Automation is Different this Time](https://youtube.com/watch?v=WSKi8HfcxEk).
* [Why AI Will Create Far More Jobs Than It Replaces](https://www.darkreading.com/careers-and-people/why-ai-will-create-far-more-jobs-than-it-replaces/a/d-id/1334635?_mc=rss_x_drr_edt_aud_dr_x_x-rss-simple)

