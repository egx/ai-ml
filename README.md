# AI and ML Research

* [What is ML?](what-is-ml.md)
* [Limitations](limitations.md)
* [Disappearing Jobs](disappearing-jobs.md)
* [Classification](classification.md)
* [Image Manipulation and Generation](image-manipulation-and-generation.md)
* [Natural Language Processing](natural-language-processing.md)
* [Videos](videos.md)
* [Southwest Cyber Security Forum Talk Notes](swcsf-talk-2019-03-04.md)
* [Interesting AI/ML Products](interesting-projects.md)
* [Best of Machine Learning](http://bestofml.com/)

## Articles

* [MIT’s depression-detecting AI might be its scariest creation yet](https://thenextweb.com/artificial-intelligence/2018/09/05/mits-depression-detecting-ai-might-be-its-scariest-creation-yet/)
* [AI winter – update – Piekniewski's blog](https://blog.piekniewski.info/2018/10/29/ai-winter-update/)
* [Autopsy of a deep learning paper – Piekniewski's blog](https://blog.piekniewski.info/2018/07/14/autopsy-dl-paper/)
* [The Genius Neuroscientist Who Might Hold the Key to True AI | WIRED](https://www.wired.com/story/karl-friston-free-energy-principle-artificial-intelligence/)
* [Is China’s corruption-busting AI system ‘Zero Trust’ being turned off for being too efficient?](https://www.scmp.com/news/china/science/article/2184857/chinas-corruption-busting-ai-system-zero-trust-being-turned-being)
* [Seven Myths in Machine Learning Research](https://crazyoscarchang.github.io/2019/02/16/seven-myths-in-machine-learning-research/)
* [These Cameras Can Spot Shoplifters Even Before They Steal](https://www.bloombergquint.com/technology/the-ai-cameras-that-can-spot-shoplifters-even-before-they-steal)
* [Why Taylor Swift Is Using Facial Recognition at Concerts](https://www.rollingstone.com/music/music-news/taylor-swift-facial-recognition-concerts-768741/)

## Self Driving Cars

* [Tesla’s Driver Fatality Rate is more than Triple that of Luxury Cars (and likely even higher)](https://medium.com/@MidwesternHedgi/teslas-driver-fatality-rate-is-more-than-triple-that-of-luxury-cars-and-likely-even-higher-433670ddde17)

## Reporting

* ['The discourse is unhinged': how the media gets AI alarmingly wrong](https://www.theguardian.com/technology/2018/jul/25/ai-artificial-intelligence-social-media-bots-wrong)
